package icfp_contest;

enum CellState {EMPTY, FULL, LOCKED};

public class Cell {

	int row;
	int col;
	
	CellState state;
	
	Cell(int cRow, int cCol, CellState cState)
	{
		this.row = cRow;
		this.col = cCol;
		this.state = cState;
	}

	public void moveSouth(){this.row++;}
	
	public void moveEast(){this.col--;}
	
	public void moveWest(){this.col++;}

}
