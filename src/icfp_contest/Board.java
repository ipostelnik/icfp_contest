package icfp_contest;

public class Board {

	int width;
	int height;
	
	Cell[][] cellArr;
	
	public Board(int cHeight, int cWidth, Cell[] filledCells) {
		// TODO Auto-generated constructor stub
		this.width = cWidth;
		this.height = cHeight;
		cellArr = new Cell[cHeight][cWidth];
		for (int cRow = 0;cRow <= cHeight;cRow ++)
			for (int cCol = 0;cCol <= cWidth;cCol ++)
			{
				cellArr[cRow][cCol] = new Cell(cRow, cCol, CellState.EMPTY);
			}
		// full cell
		for (Cell cCell : filledCells)
		{
			int cRow = cCell.row;
			int cCol = cCell.col;
			cellArr[cRow][cCol].state = CellState.FULL;
		}
	}
	
	void updateWhenFull()
	{
		int cRow = height;
		while (cRow >= 0)
		{
			if (checkForFullRow(cRow))
			{
				deleteRow(cRow);
				moveRowDown(cRow-1);
			}
			cRow --;
		}
	}
	
	boolean checkForFullRow(int cRow)
	{
		for (int cCol = 0;cCol < width;cCol ++)
		{
			if (cellArr[cRow][cCol].state == CellState.EMPTY)
				return false;
		}
		return true;
	}
	
	void deleteRow(int cRow)
	{
		// set the row to empty
		for (int cCol = 0;cCol < width;cCol ++)
		{
			cellArr[cRow][cCol].state = CellState.EMPTY;
		}
	}
	
	//move all row down start from 
	void moveRowDown(int cRow)
	{
		if (cRow < 0) return;
		if (cRow >= height) return;
		if ((cRow & 1) == 0 ) 
		{ 
			for (int cCol = 0; cCol < width; cCol++)
			{
				cellArr[cRow][cCol].moveSouth();
				cellArr[cRow][cCol].moveEast();
			}
		} else 
		{ 
			for (int cCol = 0; cCol < width; cCol++)
			{
				cellArr[cRow][cCol].moveSouth();
				cellArr[cRow][cCol].moveWest();
			}
		}
	}
}
