package icfp_contest;

public class Unit {
	Cell[] cellArray;
	Cell pivot;
	
    public Unit(Cell[] cells, Cell pivot) {
		this.cellArray = cells;
		this.pivot = pivot;
	}
    
	public void moveEast()
	{
		for(Cell cell: this.cellArray){cell.moveEast();}
		this.pivot.moveEast();
	}
	
	public void moveWest()
	{
		for(Cell cell: this.cellArray){cell.moveWest();}
		this.pivot.moveWest();
	}
	
	public void moveSouthEast()
	{
		for(Cell cell: this.cellArray){
			cell.moveSouth();
			cell.moveEast();
		}
		this.pivot.moveSouth();
		this.pivot.moveEast();
		
	}
	
	public void moveSouthWest()
	{
		for(Cell cell: this.cellArray){
			cell.moveSouth();
			cell.moveWest();
		}
		this.pivot.moveSouth();
		this.pivot.moveWest();	
	}

	public void turn()
	{
		int x,y,z;
		int relativeRow,relativeCol;
		for(Cell cell: this.cellArray){
			if((cell.row == pivot.row) && (cell.col == pivot.col)){continue;}
			relativeRow = pivot.row+cell.row;
			relativeCol = cell.col-pivot.col;
			x = relativeCol - (relativeRow-relativeRow&1)/2;
			z = relativeRow;
			y = -x -z;
			x= -z;
			y= -x;
			z= -y;
			relativeCol = x + (z - z & 1) / 2;
			relativeRow = z;
			cell.row = relativeRow;
			cell.col = relativeCol;			
		}		
	}
	
	public void turnCounter()
	{
		int x,y,z;
		int relativeRow,relativeCol;
		for(Cell cell: this.cellArray){
			if((cell.row == pivot.row) && (cell.col == pivot.col)){continue;}
			relativeRow = pivot.row+cell.row;
			relativeCol = cell.col-pivot.col;
			x = relativeCol - (relativeRow-relativeRow&1)/2;
			z = relativeRow - 0;
			y = -x -z;
			x= -y;
			y= -z;
			z= -x;
			relativeCol = x + (z - z & 1) / 2;
			relativeRow = z;
			cell.row = relativeRow;
			cell.col = relativeCol;
			
		}
	}
	
	
}
