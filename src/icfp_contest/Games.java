package icfp_contest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Games {
	static final String keyId = "id";
	static final String keyHeight = "height";
	static final String keyWidth = "width";
	static final String keySourceSeeds = "sourceSeeds";
	static final String keyUnits = "units";
	static final String keyFilled = "filled";
	static final String keySourceLength = "sourceLength";
	static final String keyMembers = "members";
	static final String keyPivot = "pivot";

	
	static Board initBoard;
	static Unit[] initUnit;
	static int sourceLength = 0;
	static int gameId = 0;
	Game[] allGames;

	Games(String pathToJsonFile) {
		String jsonData = "";

		BufferedReader br = null;

		try {
			String line;
			br = new BufferedReader(new FileReader(pathToJsonFile));
			while ((line = br.readLine()) != null) {
				jsonData += line + "\n";
			}
			
			JSONObject jsonAll = new JSONObject(jsonData);
			
			gameId = jsonAll.getInt(keyId); // Will be needed in the output
			sourceLength = jsonAll.getInt(keySourceLength);// how many unit

			// initialize the board
			int height =  jsonAll.getInt(keyHeight);
			int width = jsonAll.getInt(keyWidth);
			
			JSONArray filled = jsonAll.getJSONArray(keyFilled);
			Cell[] filledCells = new Cell[filled.length()];
			for (int i = 0; i < filled.length(); i++) {
				JSONObject filledCell = (JSONObject) filled.get(i);
				filledCells[i] = new Cell(filledCell.getInt("y"), filledCell.getInt("x"), CellState.LOCKED);
			}
			initBoard = new Board(height, width, filledCells);
			
			// extract the units
			JSONArray units = jsonAll.getJSONArray(keyUnits);
			initUnit = new Unit[units.length()];
			
			for (int i=0; i<units.length(); i++) {
				JSONObject unit = (JSONObject) units.get(i);
				JSONArray members = unit.getJSONArray(keyMembers);
				JSONObject pivot = unit.getJSONObject(keyPivot);
				
				Cell[] cells = new Cell[members.length()];
				Cell pivotCell = new Cell(pivot.getInt("y"), pivot.getInt("x"), CellState.EMPTY);
				
				for (int j = 0; j < cells.length; j++) {
					JSONObject member = (JSONObject) members.get(j);
					cells[j] = new Cell(member.getInt("y"), member.getInt("x"), CellState.FULL);
					
					System.out.println(member);
				}
				
				initUnit[i] = new Unit(cells, pivotCell);
			}
						
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public long[] generateUnitIndexes(long seed, int size){
		long[] unitIndexes= new long[size];
		long mul = 1103515245;
		long increment=12345;
		long mask=((1L << 31) - 1);
		unitIndexes[0]=(seed&mask)>>>16;
		for(int i=1; i<size;i++){
			seed = (seed * mul +increment) & mask;
			unitIndexes[i]= (int) (seed >>> (16));
		}
		
		return unitIndexes;
	}

	public static void main(String[] args) {
		new Games("problem_0.json");
	}
}
